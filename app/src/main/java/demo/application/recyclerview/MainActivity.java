
package demo.application.recyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    protected RecyclerView recyclerView;
    protected Button addTownButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
        addTownButton = findViewById(R.id.addTownButton);

        List<Town> towns = new ArrayList<>(Arrays.asList(
                new Town("London", TownType.Capital),
                new Town("Kiev", TownType.Capital),
                new Town("Kharkov", TownType.Regular)
        ));
        final TownsAdapter adapter = new TownsAdapter(towns);
        adapter.setListener(new TownsAdapterClickListener() {
            @Override
            public void onClicked(Town town) {
                Toast.makeText(MainActivity.this, town.getName(), Toast.LENGTH_LONG).show();
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        addTownButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.addTown(new Town("New Town", TownType.Regular));
            }
        });
    }
}
