package demo.application.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class TownsAdapter extends RecyclerView.Adapter<TownsAdapter.TownViewHolder> {

    protected List<Town> towns;
    protected TownsAdapterClickListener listener;

    public TownsAdapter(List<Town> towns) {
        this.towns = towns;
    }

    public void addTown(Town town) {
        towns.add(town);
        notifyDataSetChanged();
    }

    public void setListener(TownsAdapterClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public TownViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        TownType townType = TownType.values()[viewType];
        if (townType == TownType.Capital) {
            View itemView = layoutInflater.inflate(R.layout.item_town_capital, viewGroup, false);
            return new TownViewHolder(itemView);
        }
        if (townType == TownType.Regular) {
            View itemView = layoutInflater.inflate(R.layout.item_town_regular, viewGroup, false);
            return new TownViewHolder(itemView);
        }
        throw new IllegalStateException("Unknown value of TownType");
    }

    @Override
    public void onBindViewHolder(@NonNull TownViewHolder townViewHolder, int position) {
        Town town = towns.get(position);
        townViewHolder.bind(town);
    }

    @Override
    public int getItemCount() {
        return towns.size();
    }

    @Override
    public int getItemViewType(int position) {
        Town town = towns.get(position);
        return town.getType().ordinal();
    }

    class TownViewHolder extends RecyclerView.ViewHolder {

        protected TextView townTitleTextView;
        protected ImageView townImageView;

        public TownViewHolder(@NonNull View itemView) {
            super(itemView);
            townTitleTextView = itemView.findViewById(R.id.townTitleTextView);
            townImageView = itemView.findViewById(R.id.townImageView);
        }

        public void bind(final Town town) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClicked(town);
                }
            });
            townTitleTextView.setText(town.getName());
            if (town.getName().startsWith("K")) {
                townImageView.setVisibility(View.INVISIBLE);
            } else {
                townImageView.setVisibility(View.VISIBLE);
            }
        }
    }
}
