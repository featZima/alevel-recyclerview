package demo.application.recyclerview;

public class Town {

    private String name;
    private TownType type;

    public Town(String name, TownType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TownType getType() {
        return type;
    }

    public void setType(TownType type) {
        this.type = type;
    }
}
