package demo.application.recyclerview;

public interface TownsAdapterClickListener {

    void onClicked(Town town);
}
